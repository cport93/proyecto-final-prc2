﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataConexion;
using CacheManager;
using SessionManager;
namespace Mantenimientos.GUI
{
    public partial class frmGestionGrupos : Form
    {
        BindingSource _Grupos = new BindingSource(); //Todos los formularios que llevan una lista de la base de datos llevan un bindin source

        private void CargarGrupos()
        {
            Cache Datos = new Cache();
            _Grupos.DataSource = Datos.Obtener(Cache.Consultas.TODOS_LOS_GRUPOS);
            FiltrarLocalmente();
            
        }

        private void FiltrarLocalmente()
        {
            if (txtBuscar.TextLength >0)
            {
                _Grupos.Filter = "Grupo LIKE '%" +txtBuscar.Text+"%'";
            }
            else 
            {
                _Grupos.RemoveFilter();
            }
            dtgvGestionGrupos.DataSource = _Grupos;
            lblRegistrosEncontrados.Text = dtgvGestionGrupos.Rows.Count + " Registros encontrados";
        }
        public frmGestionGrupos()
        {
            InitializeComponent();
        }

        

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void frmGestionGrupos_Load(object sender, EventArgs e)
        {
            CargarGrupos();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
             frmEdicionGrupos frm = new frmEdicionGrupos();
            frm.ShowDialog();
            CargarGrupos();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Realmente desea modificar el registro?", "Pregunta", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                frmEdicionGrupos frm = new frmEdicionGrupos();
                frm.txtIDGrupo.Text = dtgvGestionGrupos.CurrentRow.Cells["IDGrupos"].Value.ToString();
                frm.txtGrupo.Text = dtgvGestionGrupos.CurrentRow.Cells["Grupo"].Value.ToString();
                frm.Show();
                CargarGrupos();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿Desea eliminar el registro?", "Aviso",MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                CLS.Grupo objGrupo = new CLS.Grupo();
                objGrupo.IDGrupo = dtgvGestionGrupos.CurrentRow.Cells["IDGrupos"].Value.ToString();
                if (objGrupo.Eliminar())
                {
                    MessageBox.Show("Registro Eliminado");
                    CargarGrupos();

                }
                else
                {
                    MessageBox.Show("El registro no fue elmiinado");
                }
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {

        }

        private void txtBuscar_Click(object sender, EventArgs e)
        {

        }
    }
}
