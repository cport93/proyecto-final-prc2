﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataConexion;
using CacheManager;
using SessionManager;
namespace Mantenimientos.GUI
{
    public partial class frmGestionUsuarios : Form
          
    {
        BindingSource _Usuario = new BindingSource();
        private void CargarGrupos()
        {
            Cache Datos = new Cache();
            _Usuario.DataSource = Datos.Obtener(Cache.Consultas.TODOS_LOS_USUARIOS);
            FiltrarLocalmente();
        }

        private void FiltrarLocalmente()
        {
            if (txtBuscar.TextLength > 0)
            {
                _Usuario.Filter = "Usuario LIKE '%" + txtBuscar.Text + "%'";
            }
            else
            {
                _Usuario.RemoveFilter();
            }
            dtgvGestionUsuarios.DataSource = _Usuario;
            lblRegistrosEncontrados.Text = dtgvGestionUsuarios.Rows.Count + " Registros encontrados";
        }
        public frmGestionUsuarios()
        {
            InitializeComponent();
        }

        private void dtgvGestionEquipos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void frmGestionUsuarios_Load(object sender, EventArgs e)
        {
            CargarGrupos();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            Mantenimientos.GUI.frmEdicionUsuarios frm = new Mantenimientos.GUI.frmEdicionUsuarios();
            frm.ShowDialog();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea modificar el registro?", "Pregunta", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                frmEdicionUsuarios frm = new frmEdicionUsuarios();
                frm.txtIDUsuario.Text = dtgvGestionUsuarios.CurrentRow.Cells["IDUsuario"].Value.ToString();
                frm.txtUsuario.Text = dtgvGestionUsuarios.CurrentRow.Cells["Usuario"].Value.ToString();
                frm.txtCredencial.Text = dtgvGestionUsuarios.CurrentRow.Cells["Credencial"].Value.ToString();
                frm.cbIDEmpleado.DisplayMember = dtgvGestionUsuarios.CurrentRow.Cells["IDEmpleado"].Value.ToString();
                frm.cbIDGrupoU.DisplayMember = dtgvGestionUsuarios.CurrentRow.Cells["IDGrupo"].Value.ToString();
                frm.txtFechaCreacion.Text = dtgvGestionUsuarios.CurrentRow.Cells["FechaCreacion"].Value.ToString();

                frm.Show();
                CargarGrupos();
            }
        }
    }
}
