﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mantenimientos.GUI
{
    public partial class frmEdicionEquipos : Form
    {
        public frmEdicionEquipos()
        {
            InitializeComponent();
        }
        private void Procesar()
        {
            //Creamos el objeto grupo vacio
            CLS.Equipo objEquipo = new CLS.Equipo();
            //Se sincroniza el objeto
            objEquipo.IDEquipo = txtIDEquipo.Text;
            objEquipo.NombreEquipo = txtNombreEquipo.Text;
            {
                if (txtIDEquipo.TextLength > 0)
                {
                    //Estamos modificando
                    if (objEquipo.Actualizar())
                    {
                        MessageBox.Show("Registro actualizado", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Registro no fue actualizado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    MessageBox.Show("Registro insertado", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    objEquipo.Guardar();
                    Close();
                }
            }

        }
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtGrupo_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            DataTable resultado = new DataTable();
            DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
            resultado = Operador.Consultar("SELECT max(IDEquipo) FROM sistema.equipos ;");
            DataRow linea = resultado.Rows[0];
            String Limite = linea[0].ToString();
            if (int.Parse(Limite) < 17)
            {
                Procesar();
               
            }
            else
            {
                MessageBox.Show("No puede ingresar más equipos, por favor elimine uno", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            
        }
    }
}
