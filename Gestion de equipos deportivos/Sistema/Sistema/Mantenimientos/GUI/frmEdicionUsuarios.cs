﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mantenimientos.GUI
{
    public partial class frmEdicionUsuarios : Form
    {
        public frmEdicionUsuarios()
        {
            InitializeComponent();
        }
        private void Procesar()
        {
            //Creamos el objeto grupo vacio
            CLS.Usuario ObjUsuario = new CLS.Usuario();
            //Se sincroniza el objeto
            ObjUsuario.IDUsuario = txtIDUsuario.Text;
            ObjUsuario.NombreUsuario = txtUsuario.Text;
            ObjUsuario.Credencial = txtCredencial.Text;
            ObjUsuario.IDEmpleado = cbIDEmpleado.Text;
            ObjUsuario.IDGrupo = cbIDGrupoU.Text;
            ObjUsuario.FechaCreacion = txtFechaCreacion.Text;

            if (txtIDUsuario.TextLength > 0)
            {

                if (ObjUsuario.Actualizar())
                {
                    MessageBox.Show("Registro actualizado", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    Close();
                }
                else
                {
                    MessageBox.Show("Registro no fue actualizado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("Registro insertado", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                ObjUsuario.Guardar();
                Close();
            }
        }

        private void frmEdicionUsuarios_Load(object sender, EventArgs e)
        {
            DataTable resultado1 = new DataTable();
            DataTable resultado2 = new DataTable();
            DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
            resultado1 = Operador.Consultar("SELECT IDEmpleado FROM sistema.empleados;");
            resultado2 = Operador.Consultar("SELECT IDGrupos FROM sistema.grupos;");
            cbIDEmpleado.DataSource = resultado1;
            cbIDGrupoU.DataSource = resultado2;
            cbIDEmpleado.DisplayMember = "IDEmpleado";
            cbIDEmpleado.ValueMember = "IDEmpleado";
            cbIDGrupoU.DisplayMember = "IDGrupos";
            cbIDGrupoU.ValueMember = "IDGrupos";
            this.cbIDEmpleado.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbIDGrupoU.DropDownStyle = ComboBoxStyle.DropDownList;

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Procesar();
        }

        private void cbIDGrupoU_TextChanged(object sender, EventArgs e)
        {
            var row = (DataRowView)cbIDGrupoU.SelectedItem;
        
            DataTable resultado = new DataTable();
            DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
            resultado = Operador.Consultar("select g.Grupo from usuarios u inner join grupos g on u.IDGrupo = g.IDGrupos where u.IDGrupo ="+row[0]+";");
            DataRow linea = resultado.Rows[0];
            txtIDGrupo.Text = linea[0].ToString();
        }
    }
}
