﻿namespace Mantenimientos.GUI
{
    partial class frmEdicionUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.cbIDEmpleado = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.Grupo = new System.Windows.Forms.Label();
            this.txtIDUsuario = new System.Windows.Forms.TextBox();
            this.lblIDUsuario = new System.Windows.Forms.Label();
            this.txtCredencial = new System.Windows.Forms.TextBox();
            this.cbIDGrupoU = new System.Windows.Forms.ComboBox();
            this.txtFechaCreacion = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtIDGrupo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 37;
            this.label4.Text = "ID Grupo";
            // 
            // cbIDEmpleado
            // 
            this.cbIDEmpleado.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbIDEmpleado.FormattingEnabled = true;
            this.cbIDEmpleado.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbIDEmpleado.Location = new System.Drawing.Point(112, 135);
            this.cbIDEmpleado.Name = "cbIDEmpleado";
            this.cbIDEmpleado.Size = new System.Drawing.Size(121, 21);
            this.cbIDEmpleado.TabIndex = 36;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "ID Empleado";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Credencial";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(160, 249);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 33;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(63, 249);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 32;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(112, 56);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(194, 20);
            this.txtUsuario.TabIndex = 31;
            // 
            // Grupo
            // 
            this.Grupo.AutoSize = true;
            this.Grupo.Location = new System.Drawing.Point(40, 59);
            this.Grupo.Name = "Grupo";
            this.Grupo.Size = new System.Drawing.Size(43, 13);
            this.Grupo.TabIndex = 30;
            this.Grupo.Text = "Usuario";
            // 
            // txtIDUsuario
            // 
            this.txtIDUsuario.Enabled = false;
            this.txtIDUsuario.Location = new System.Drawing.Point(112, 21);
            this.txtIDUsuario.Name = "txtIDUsuario";
            this.txtIDUsuario.Size = new System.Drawing.Size(194, 20);
            this.txtIDUsuario.TabIndex = 29;
            // 
            // lblIDUsuario
            // 
            this.lblIDUsuario.AutoSize = true;
            this.lblIDUsuario.Location = new System.Drawing.Point(26, 28);
            this.lblIDUsuario.Name = "lblIDUsuario";
            this.lblIDUsuario.Size = new System.Drawing.Size(57, 13);
            this.lblIDUsuario.TabIndex = 28;
            this.lblIDUsuario.Text = "ID Usuario";
            // 
            // txtCredencial
            // 
            this.txtCredencial.Location = new System.Drawing.Point(112, 98);
            this.txtCredencial.Name = "txtCredencial";
            this.txtCredencial.PasswordChar = '*';
            this.txtCredencial.Size = new System.Drawing.Size(194, 20);
            this.txtCredencial.TabIndex = 39;
            // 
            // cbIDGrupoU
            // 
            this.cbIDGrupoU.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbIDGrupoU.FormattingEnabled = true;
            this.cbIDGrupoU.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbIDGrupoU.Location = new System.Drawing.Point(112, 173);
            this.cbIDGrupoU.Name = "cbIDGrupoU";
            this.cbIDGrupoU.Size = new System.Drawing.Size(121, 21);
            this.cbIDGrupoU.TabIndex = 40;
            this.cbIDGrupoU.TextChanged += new System.EventHandler(this.cbIDGrupoU_TextChanged);
            // 
            // txtFechaCreacion
            // 
            this.txtFechaCreacion.Enabled = false;
            this.txtFechaCreacion.Location = new System.Drawing.Point(112, 214);
            this.txtFechaCreacion.Name = "txtFechaCreacion";
            this.txtFechaCreacion.Size = new System.Drawing.Size(194, 20);
            this.txtFechaCreacion.TabIndex = 42;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 208);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 26);
            this.label1.TabIndex = 41;
            this.label1.Text = "Fecha de\r\nCreación";
            // 
            // txtIDGrupo
            // 
            this.txtIDGrupo.Enabled = false;
            this.txtIDGrupo.Location = new System.Drawing.Point(240, 173);
            this.txtIDGrupo.Name = "txtIDGrupo";
            this.txtIDGrupo.Size = new System.Drawing.Size(100, 20);
            this.txtIDGrupo.TabIndex = 43;
            // 
            // frmEdicionUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 284);
            this.Controls.Add(this.txtIDGrupo);
            this.Controls.Add(this.txtFechaCreacion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbIDGrupoU);
            this.Controls.Add(this.txtCredencial);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbIDEmpleado);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtUsuario);
            this.Controls.Add(this.Grupo);
            this.Controls.Add(this.txtIDUsuario);
            this.Controls.Add(this.lblIDUsuario);
            this.Name = "frmEdicionUsuarios";
            this.Text = "frmEdicionUsuarios";
            this.Load += new System.EventHandler(this.frmEdicionUsuarios_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox cbIDEmpleado;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        public System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.Label Grupo;
        public System.Windows.Forms.TextBox txtIDUsuario;
        private System.Windows.Forms.Label lblIDUsuario;
        public System.Windows.Forms.TextBox txtCredencial;
        public System.Windows.Forms.ComboBox cbIDGrupoU;
        public System.Windows.Forms.TextBox txtFechaCreacion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIDGrupo;
    }
}