﻿namespace Mantenimientos.GUI
{
    partial class frmClasificacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmClasificacion));
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.txtOctavos16 = new System.Windows.Forms.TextBox();
            this.txtOctavos15 = new System.Windows.Forms.TextBox();
            this.txtOctavos14 = new System.Windows.Forms.TextBox();
            this.txtOctavos13 = new System.Windows.Forms.TextBox();
            this.txtOctavos12 = new System.Windows.Forms.TextBox();
            this.txtOctavos11 = new System.Windows.Forms.TextBox();
            this.txtOctavos10 = new System.Windows.Forms.TextBox();
            this.txtOctavos9 = new System.Windows.Forms.TextBox();
            this.txtOctavos8 = new System.Windows.Forms.TextBox();
            this.txtOctavos7 = new System.Windows.Forms.TextBox();
            this.txtOctavos6 = new System.Windows.Forms.TextBox();
            this.txtOctavos5 = new System.Windows.Forms.TextBox();
            this.txtOctavos4 = new System.Windows.Forms.TextBox();
            this.txtOctavos3 = new System.Windows.Forms.TextBox();
            this.txtOctavos2 = new System.Windows.Forms.TextBox();
            this.txtOctavos1 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.cbAño = new System.Windows.Forms.ComboBox();
            this.cbFinal2 = new System.Windows.Forms.ComboBox();
            this.cbFinal1 = new System.Windows.Forms.ComboBox();
            this.cbSemis4 = new System.Windows.Forms.ComboBox();
            this.cbSemis3 = new System.Windows.Forms.ComboBox();
            this.cbSemis2 = new System.Windows.Forms.ComboBox();
            this.cbSemis1 = new System.Windows.Forms.ComboBox();
            this.cbCuartos8 = new System.Windows.Forms.ComboBox();
            this.cbCuartos6 = new System.Windows.Forms.ComboBox();
            this.cbCuartos7 = new System.Windows.Forms.ComboBox();
            this.cbCuartos5 = new System.Windows.Forms.ComboBox();
            this.cbCuartos4 = new System.Windows.Forms.ComboBox();
            this.cbCuartos2 = new System.Windows.Forms.ComboBox();
            this.cbCuartos3 = new System.Windows.Forms.ComboBox();
            this.cbCuartos1 = new System.Windows.Forms.ComboBox();
            this.cbOctavos16 = new System.Windows.Forms.ComboBox();
            this.cbOctavos15 = new System.Windows.Forms.ComboBox();
            this.cbOctavos14 = new System.Windows.Forms.ComboBox();
            this.cbOctavos13 = new System.Windows.Forms.ComboBox();
            this.cbOctavos12 = new System.Windows.Forms.ComboBox();
            this.cbOctavos11 = new System.Windows.Forms.ComboBox();
            this.cbOctavos10 = new System.Windows.Forms.ComboBox();
            this.cbOctavos9 = new System.Windows.Forms.ComboBox();
            this.cbOctavos8 = new System.Windows.Forms.ComboBox();
            this.cbOctavos7 = new System.Windows.Forms.ComboBox();
            this.cbOctavos6 = new System.Windows.Forms.ComboBox();
            this.cbOctavos5 = new System.Windows.Forms.ComboBox();
            this.cbOctavos4 = new System.Windows.Forms.ComboBox();
            this.cbOctavos3 = new System.Windows.Forms.ComboBox();
            this.cbOctavos2 = new System.Windows.Forms.ComboBox();
            this.cbOctavos1 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(737, 404);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(20, 20);
            this.textBox31.TabIndex = 175;
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(737, 179);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(20, 20);
            this.textBox32.TabIndex = 174;
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(613, 297);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(20, 20);
            this.textBox30.TabIndex = 173;
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(584, 296);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(20, 20);
            this.textBox29.TabIndex = 172;
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(456, 404);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(20, 20);
            this.textBox28.TabIndex = 171;
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(456, 179);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(20, 20);
            this.textBox27.TabIndex = 170;
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(299, 133);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(20, 20);
            this.textBox23.TabIndex = 169;
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(299, 242);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(20, 20);
            this.textBox24.TabIndex = 168;
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(299, 346);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(20, 20);
            this.textBox25.TabIndex = 167;
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(299, 458);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(20, 20);
            this.textBox26.TabIndex = 166;
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(894, 133);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(20, 20);
            this.textBox22.TabIndex = 165;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(894, 242);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(20, 20);
            this.textBox21.TabIndex = 164;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(894, 346);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(20, 20);
            this.textBox20.TabIndex = 163;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(894, 458);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(20, 20);
            this.textBox19.TabIndex = 162;
            // 
            // txtOctavos16
            // 
            this.txtOctavos16.Location = new System.Drawing.Point(1062, 488);
            this.txtOctavos16.Name = "txtOctavos16";
            this.txtOctavos16.Size = new System.Drawing.Size(20, 20);
            this.txtOctavos16.TabIndex = 161;
            this.txtOctavos16.TextChanged += new System.EventHandler(this.txtOctavos16_TextChanged);
            // 
            // txtOctavos15
            // 
            this.txtOctavos15.Location = new System.Drawing.Point(1062, 434);
            this.txtOctavos15.Name = "txtOctavos15";
            this.txtOctavos15.Size = new System.Drawing.Size(21, 20);
            this.txtOctavos15.TabIndex = 160;
            // 
            // txtOctavos14
            // 
            this.txtOctavos14.Location = new System.Drawing.Point(1062, 374);
            this.txtOctavos14.Name = "txtOctavos14";
            this.txtOctavos14.Size = new System.Drawing.Size(22, 20);
            this.txtOctavos14.TabIndex = 159;
            this.txtOctavos14.TextChanged += new System.EventHandler(this.txtOctavos14_TextChanged);
            // 
            // txtOctavos13
            // 
            this.txtOctavos13.Location = new System.Drawing.Point(1062, 320);
            this.txtOctavos13.Name = "txtOctavos13";
            this.txtOctavos13.Size = new System.Drawing.Size(22, 20);
            this.txtOctavos13.TabIndex = 158;
            // 
            // txtOctavos12
            // 
            this.txtOctavos12.Location = new System.Drawing.Point(1063, 267);
            this.txtOctavos12.Name = "txtOctavos12";
            this.txtOctavos12.Size = new System.Drawing.Size(22, 20);
            this.txtOctavos12.TabIndex = 157;
            this.txtOctavos12.TextChanged += new System.EventHandler(this.txtOctavos12_TextChanged);
            // 
            // txtOctavos11
            // 
            this.txtOctavos11.Location = new System.Drawing.Point(1062, 212);
            this.txtOctavos11.Name = "txtOctavos11";
            this.txtOctavos11.Size = new System.Drawing.Size(23, 20);
            this.txtOctavos11.TabIndex = 156;
            // 
            // txtOctavos10
            // 
            this.txtOctavos10.Location = new System.Drawing.Point(1062, 156);
            this.txtOctavos10.Name = "txtOctavos10";
            this.txtOctavos10.Size = new System.Drawing.Size(22, 20);
            this.txtOctavos10.TabIndex = 155;
            this.txtOctavos10.TextChanged += new System.EventHandler(this.txtOctavos10_TextChanged);
            // 
            // txtOctavos9
            // 
            this.txtOctavos9.Location = new System.Drawing.Point(1062, 106);
            this.txtOctavos9.Name = "txtOctavos9";
            this.txtOctavos9.Size = new System.Drawing.Size(23, 20);
            this.txtOctavos9.TabIndex = 154;
            // 
            // txtOctavos8
            // 
            this.txtOctavos8.Location = new System.Drawing.Point(144, 488);
            this.txtOctavos8.Name = "txtOctavos8";
            this.txtOctavos8.Size = new System.Drawing.Size(20, 20);
            this.txtOctavos8.TabIndex = 153;
            this.txtOctavos8.TextChanged += new System.EventHandler(this.txtOctavos8_TextChanged);
            // 
            // txtOctavos7
            // 
            this.txtOctavos7.Location = new System.Drawing.Point(144, 434);
            this.txtOctavos7.Name = "txtOctavos7";
            this.txtOctavos7.Size = new System.Drawing.Size(21, 20);
            this.txtOctavos7.TabIndex = 152;
            // 
            // txtOctavos6
            // 
            this.txtOctavos6.Location = new System.Drawing.Point(144, 374);
            this.txtOctavos6.Name = "txtOctavos6";
            this.txtOctavos6.Size = new System.Drawing.Size(22, 20);
            this.txtOctavos6.TabIndex = 151;
            this.txtOctavos6.TextChanged += new System.EventHandler(this.txtOctavos6_TextChanged);
            // 
            // txtOctavos5
            // 
            this.txtOctavos5.Location = new System.Drawing.Point(144, 320);
            this.txtOctavos5.Name = "txtOctavos5";
            this.txtOctavos5.Size = new System.Drawing.Size(22, 20);
            this.txtOctavos5.TabIndex = 150;
            // 
            // txtOctavos4
            // 
            this.txtOctavos4.Location = new System.Drawing.Point(145, 267);
            this.txtOctavos4.Name = "txtOctavos4";
            this.txtOctavos4.Size = new System.Drawing.Size(22, 20);
            this.txtOctavos4.TabIndex = 149;
            this.txtOctavos4.TextChanged += new System.EventHandler(this.txtOctavos4_TextChanged);
            // 
            // txtOctavos3
            // 
            this.txtOctavos3.Location = new System.Drawing.Point(144, 212);
            this.txtOctavos3.Name = "txtOctavos3";
            this.txtOctavos3.Size = new System.Drawing.Size(23, 20);
            this.txtOctavos3.TabIndex = 148;
            // 
            // txtOctavos2
            // 
            this.txtOctavos2.Location = new System.Drawing.Point(144, 156);
            this.txtOctavos2.Name = "txtOctavos2";
            this.txtOctavos2.Size = new System.Drawing.Size(22, 20);
            this.txtOctavos2.TabIndex = 147;
            this.txtOctavos2.TextChanged += new System.EventHandler(this.txtOctavos2_TextChanged);
            // 
            // txtOctavos1
            // 
            this.txtOctavos1.Location = new System.Drawing.Point(144, 106);
            this.txtOctavos1.Name = "txtOctavos1";
            this.txtOctavos1.Size = new System.Drawing.Size(23, 20);
            this.txtOctavos1.TabIndex = 146;
            this.txtOctavos1.TextChanged += new System.EventHandler(this.txtOctavos1_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(584, 121);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(49, 55);
            this.pictureBox1.TabIndex = 145;
            this.pictureBox1.TabStop = false;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(532, 53);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(150, 27);
            this.textBox2.TabIndex = 144;
            this.textBox2.Text = "Copa PRC 2016";
            // 
            // cbAño
            // 
            this.cbAño.FormattingEnabled = true;
            this.cbAño.Location = new System.Drawing.Point(543, 211);
            this.cbAño.Name = "cbAño";
            this.cbAño.Size = new System.Drawing.Size(121, 21);
            this.cbAño.TabIndex = 143;
            // 
            // cbFinal2
            // 
            this.cbFinal2.Enabled = false;
            this.cbFinal2.FormattingEnabled = true;
            this.cbFinal2.Location = new System.Drawing.Point(639, 296);
            this.cbFinal2.Name = "cbFinal2";
            this.cbFinal2.Size = new System.Drawing.Size(121, 21);
            this.cbFinal2.TabIndex = 142;
            // 
            // cbFinal1
            // 
            this.cbFinal1.Enabled = false;
            this.cbFinal1.FormattingEnabled = true;
            this.cbFinal1.Location = new System.Drawing.Point(456, 296);
            this.cbFinal1.Name = "cbFinal1";
            this.cbFinal1.Size = new System.Drawing.Size(121, 21);
            this.cbFinal1.TabIndex = 141;
            // 
            // cbSemis4
            // 
            this.cbSemis4.Enabled = false;
            this.cbSemis4.FormattingEnabled = true;
            this.cbSemis4.Location = new System.Drawing.Point(763, 404);
            this.cbSemis4.Name = "cbSemis4";
            this.cbSemis4.Size = new System.Drawing.Size(121, 21);
            this.cbSemis4.TabIndex = 140;
            // 
            // cbSemis3
            // 
            this.cbSemis3.Enabled = false;
            this.cbSemis3.FormattingEnabled = true;
            this.cbSemis3.Location = new System.Drawing.Point(763, 179);
            this.cbSemis3.Name = "cbSemis3";
            this.cbSemis3.Size = new System.Drawing.Size(121, 21);
            this.cbSemis3.TabIndex = 139;
            // 
            // cbSemis2
            // 
            this.cbSemis2.Enabled = false;
            this.cbSemis2.FormattingEnabled = true;
            this.cbSemis2.Location = new System.Drawing.Point(329, 404);
            this.cbSemis2.Name = "cbSemis2";
            this.cbSemis2.Size = new System.Drawing.Size(121, 21);
            this.cbSemis2.TabIndex = 138;
            // 
            // cbSemis1
            // 
            this.cbSemis1.Enabled = false;
            this.cbSemis1.FormattingEnabled = true;
            this.cbSemis1.Location = new System.Drawing.Point(329, 178);
            this.cbSemis1.Name = "cbSemis1";
            this.cbSemis1.Size = new System.Drawing.Size(121, 21);
            this.cbSemis1.TabIndex = 137;
            // 
            // cbCuartos8
            // 
            this.cbCuartos8.Enabled = false;
            this.cbCuartos8.FormattingEnabled = true;
            this.cbCuartos8.Location = new System.Drawing.Point(920, 457);
            this.cbCuartos8.Name = "cbCuartos8";
            this.cbCuartos8.Size = new System.Drawing.Size(121, 21);
            this.cbCuartos8.TabIndex = 136;
            // 
            // cbCuartos6
            // 
            this.cbCuartos6.Enabled = false;
            this.cbCuartos6.FormattingEnabled = true;
            this.cbCuartos6.Location = new System.Drawing.Point(920, 241);
            this.cbCuartos6.Name = "cbCuartos6";
            this.cbCuartos6.Size = new System.Drawing.Size(121, 21);
            this.cbCuartos6.TabIndex = 135;
            // 
            // cbCuartos7
            // 
            this.cbCuartos7.Enabled = false;
            this.cbCuartos7.FormattingEnabled = true;
            this.cbCuartos7.Location = new System.Drawing.Point(920, 345);
            this.cbCuartos7.Name = "cbCuartos7";
            this.cbCuartos7.Size = new System.Drawing.Size(121, 21);
            this.cbCuartos7.TabIndex = 134;
            // 
            // cbCuartos5
            // 
            this.cbCuartos5.Enabled = false;
            this.cbCuartos5.FormattingEnabled = true;
            this.cbCuartos5.Location = new System.Drawing.Point(920, 132);
            this.cbCuartos5.Name = "cbCuartos5";
            this.cbCuartos5.Size = new System.Drawing.Size(121, 21);
            this.cbCuartos5.TabIndex = 133;
            // 
            // cbCuartos4
            // 
            this.cbCuartos4.Enabled = false;
            this.cbCuartos4.FormattingEnabled = true;
            this.cbCuartos4.Location = new System.Drawing.Point(172, 457);
            this.cbCuartos4.Name = "cbCuartos4";
            this.cbCuartos4.Size = new System.Drawing.Size(121, 21);
            this.cbCuartos4.TabIndex = 132;
            // 
            // cbCuartos2
            // 
            this.cbCuartos2.Enabled = false;
            this.cbCuartos2.FormattingEnabled = true;
            this.cbCuartos2.Location = new System.Drawing.Point(172, 241);
            this.cbCuartos2.Name = "cbCuartos2";
            this.cbCuartos2.Size = new System.Drawing.Size(121, 21);
            this.cbCuartos2.TabIndex = 131;
            // 
            // cbCuartos3
            // 
            this.cbCuartos3.Enabled = false;
            this.cbCuartos3.FormattingEnabled = true;
            this.cbCuartos3.Location = new System.Drawing.Point(172, 345);
            this.cbCuartos3.Name = "cbCuartos3";
            this.cbCuartos3.Size = new System.Drawing.Size(121, 21);
            this.cbCuartos3.TabIndex = 130;
            // 
            // cbCuartos1
            // 
            this.cbCuartos1.Enabled = false;
            this.cbCuartos1.FormattingEnabled = true;
            this.cbCuartos1.Location = new System.Drawing.Point(172, 132);
            this.cbCuartos1.Name = "cbCuartos1";
            this.cbCuartos1.Size = new System.Drawing.Size(121, 21);
            this.cbCuartos1.TabIndex = 129;
            // 
            // cbOctavos16
            // 
            this.cbOctavos16.Enabled = false;
            this.cbOctavos16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbOctavos16.FormattingEnabled = true;
            this.cbOctavos16.Location = new System.Drawing.Point(1090, 488);
            this.cbOctavos16.Name = "cbOctavos16";
            this.cbOctavos16.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos16.TabIndex = 128;
            // 
            // cbOctavos15
            // 
            this.cbOctavos15.Enabled = false;
            this.cbOctavos15.FormattingEnabled = true;
            this.cbOctavos15.Location = new System.Drawing.Point(1090, 434);
            this.cbOctavos15.Name = "cbOctavos15";
            this.cbOctavos15.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos15.TabIndex = 127;
            // 
            // cbOctavos14
            // 
            this.cbOctavos14.Enabled = false;
            this.cbOctavos14.FormattingEnabled = true;
            this.cbOctavos14.Location = new System.Drawing.Point(1090, 374);
            this.cbOctavos14.Name = "cbOctavos14";
            this.cbOctavos14.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos14.TabIndex = 126;
            // 
            // cbOctavos13
            // 
            this.cbOctavos13.Enabled = false;
            this.cbOctavos13.FormattingEnabled = true;
            this.cbOctavos13.Location = new System.Drawing.Point(1090, 320);
            this.cbOctavos13.Name = "cbOctavos13";
            this.cbOctavos13.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos13.TabIndex = 125;
            // 
            // cbOctavos12
            // 
            this.cbOctavos12.Enabled = false;
            this.cbOctavos12.FormattingEnabled = true;
            this.cbOctavos12.Location = new System.Drawing.Point(1090, 267);
            this.cbOctavos12.Name = "cbOctavos12";
            this.cbOctavos12.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos12.TabIndex = 124;
            // 
            // cbOctavos11
            // 
            this.cbOctavos11.Enabled = false;
            this.cbOctavos11.FormattingEnabled = true;
            this.cbOctavos11.Location = new System.Drawing.Point(1090, 212);
            this.cbOctavos11.Name = "cbOctavos11";
            this.cbOctavos11.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos11.TabIndex = 123;
            // 
            // cbOctavos10
            // 
            this.cbOctavos10.Enabled = false;
            this.cbOctavos10.FormattingEnabled = true;
            this.cbOctavos10.Location = new System.Drawing.Point(1090, 156);
            this.cbOctavos10.Name = "cbOctavos10";
            this.cbOctavos10.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos10.TabIndex = 122;
            // 
            // cbOctavos9
            // 
            this.cbOctavos9.Enabled = false;
            this.cbOctavos9.FormattingEnabled = true;
            this.cbOctavos9.Location = new System.Drawing.Point(1090, 106);
            this.cbOctavos9.Name = "cbOctavos9";
            this.cbOctavos9.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos9.TabIndex = 121;
            // 
            // cbOctavos8
            // 
            this.cbOctavos8.Enabled = false;
            this.cbOctavos8.FormattingEnabled = true;
            this.cbOctavos8.Location = new System.Drawing.Point(17, 488);
            this.cbOctavos8.Name = "cbOctavos8";
            this.cbOctavos8.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos8.TabIndex = 120;
            // 
            // cbOctavos7
            // 
            this.cbOctavos7.Enabled = false;
            this.cbOctavos7.FormattingEnabled = true;
            this.cbOctavos7.Location = new System.Drawing.Point(17, 434);
            this.cbOctavos7.Name = "cbOctavos7";
            this.cbOctavos7.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos7.TabIndex = 119;
            // 
            // cbOctavos6
            // 
            this.cbOctavos6.Enabled = false;
            this.cbOctavos6.FormattingEnabled = true;
            this.cbOctavos6.Location = new System.Drawing.Point(17, 374);
            this.cbOctavos6.Name = "cbOctavos6";
            this.cbOctavos6.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos6.TabIndex = 118;
            // 
            // cbOctavos5
            // 
            this.cbOctavos5.Enabled = false;
            this.cbOctavos5.FormattingEnabled = true;
            this.cbOctavos5.Location = new System.Drawing.Point(17, 320);
            this.cbOctavos5.Name = "cbOctavos5";
            this.cbOctavos5.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos5.TabIndex = 117;
            this.cbOctavos5.TextChanged += new System.EventHandler(this.cbOctavos5_TextChanged);
            // 
            // cbOctavos4
            // 
            this.cbOctavos4.Enabled = false;
            this.cbOctavos4.FormattingEnabled = true;
            this.cbOctavos4.Location = new System.Drawing.Point(17, 267);
            this.cbOctavos4.Name = "cbOctavos4";
            this.cbOctavos4.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos4.TabIndex = 116;
            this.cbOctavos4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbOctavos4_MouseClick);
            // 
            // cbOctavos3
            // 
            this.cbOctavos3.Enabled = false;
            this.cbOctavos3.FormattingEnabled = true;
            this.cbOctavos3.Location = new System.Drawing.Point(17, 212);
            this.cbOctavos3.Name = "cbOctavos3";
            this.cbOctavos3.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos3.TabIndex = 115;
            this.cbOctavos3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbOctavos3_MouseClick);
            // 
            // cbOctavos2
            // 
            this.cbOctavos2.Enabled = false;
            this.cbOctavos2.FormattingEnabled = true;
            this.cbOctavos2.Location = new System.Drawing.Point(17, 156);
            this.cbOctavos2.Name = "cbOctavos2";
            this.cbOctavos2.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos2.TabIndex = 114;
            this.cbOctavos2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbOctavos2_MouseClick);
            // 
            // cbOctavos1
            // 
            this.cbOctavos1.Enabled = false;
            this.cbOctavos1.FormattingEnabled = true;
            this.cbOctavos1.Location = new System.Drawing.Point(17, 106);
            this.cbOctavos1.Name = "cbOctavos1";
            this.cbOctavos1.Size = new System.Drawing.Size(121, 21);
            this.cbOctavos1.TabIndex = 113;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(517, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(176, 35);
            this.textBox1.TabIndex = 112;
            this.textBox1.Text = "Clasificacion";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(517, 86);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(186, 23);
            this.button1.TabIndex = 176;
            this.button1.Text = "Generar Octavos Aleatoramente";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmClasificacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1217, 543);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox31);
            this.Controls.Add(this.textBox32);
            this.Controls.Add(this.textBox30);
            this.Controls.Add(this.textBox29);
            this.Controls.Add(this.textBox28);
            this.Controls.Add(this.textBox27);
            this.Controls.Add(this.textBox23);
            this.Controls.Add(this.textBox24);
            this.Controls.Add(this.textBox25);
            this.Controls.Add(this.textBox26);
            this.Controls.Add(this.textBox22);
            this.Controls.Add(this.textBox21);
            this.Controls.Add(this.textBox20);
            this.Controls.Add(this.textBox19);
            this.Controls.Add(this.txtOctavos16);
            this.Controls.Add(this.txtOctavos15);
            this.Controls.Add(this.txtOctavos14);
            this.Controls.Add(this.txtOctavos13);
            this.Controls.Add(this.txtOctavos12);
            this.Controls.Add(this.txtOctavos11);
            this.Controls.Add(this.txtOctavos10);
            this.Controls.Add(this.txtOctavos9);
            this.Controls.Add(this.txtOctavos8);
            this.Controls.Add(this.txtOctavos7);
            this.Controls.Add(this.txtOctavos6);
            this.Controls.Add(this.txtOctavos5);
            this.Controls.Add(this.txtOctavos4);
            this.Controls.Add(this.txtOctavos3);
            this.Controls.Add(this.txtOctavos2);
            this.Controls.Add(this.txtOctavos1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.cbAño);
            this.Controls.Add(this.cbFinal2);
            this.Controls.Add(this.cbFinal1);
            this.Controls.Add(this.cbSemis4);
            this.Controls.Add(this.cbSemis3);
            this.Controls.Add(this.cbSemis2);
            this.Controls.Add(this.cbSemis1);
            this.Controls.Add(this.cbCuartos8);
            this.Controls.Add(this.cbCuartos6);
            this.Controls.Add(this.cbCuartos7);
            this.Controls.Add(this.cbCuartos5);
            this.Controls.Add(this.cbCuartos4);
            this.Controls.Add(this.cbCuartos2);
            this.Controls.Add(this.cbCuartos3);
            this.Controls.Add(this.cbCuartos1);
            this.Controls.Add(this.cbOctavos16);
            this.Controls.Add(this.cbOctavos15);
            this.Controls.Add(this.cbOctavos14);
            this.Controls.Add(this.cbOctavos13);
            this.Controls.Add(this.cbOctavos12);
            this.Controls.Add(this.cbOctavos11);
            this.Controls.Add(this.cbOctavos10);
            this.Controls.Add(this.cbOctavos9);
            this.Controls.Add(this.cbOctavos8);
            this.Controls.Add(this.cbOctavos7);
            this.Controls.Add(this.cbOctavos6);
            this.Controls.Add(this.cbOctavos5);
            this.Controls.Add(this.cbOctavos4);
            this.Controls.Add(this.cbOctavos3);
            this.Controls.Add(this.cbOctavos2);
            this.Controls.Add(this.cbOctavos1);
            this.Controls.Add(this.textBox1);
            this.Name = "frmClasificacion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frmClasificacion_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frmClasificacion_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox txtOctavos16;
        private System.Windows.Forms.TextBox txtOctavos15;
        private System.Windows.Forms.TextBox txtOctavos14;
        private System.Windows.Forms.TextBox txtOctavos13;
        private System.Windows.Forms.TextBox txtOctavos12;
        private System.Windows.Forms.TextBox txtOctavos11;
        private System.Windows.Forms.TextBox txtOctavos10;
        private System.Windows.Forms.TextBox txtOctavos9;
        private System.Windows.Forms.TextBox txtOctavos8;
        private System.Windows.Forms.TextBox txtOctavos7;
        private System.Windows.Forms.TextBox txtOctavos6;
        private System.Windows.Forms.TextBox txtOctavos5;
        private System.Windows.Forms.TextBox txtOctavos4;
        private System.Windows.Forms.TextBox txtOctavos3;
        private System.Windows.Forms.TextBox txtOctavos2;
        private System.Windows.Forms.TextBox txtOctavos1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox cbAño;
        private System.Windows.Forms.ComboBox cbFinal2;
        private System.Windows.Forms.ComboBox cbFinal1;
        private System.Windows.Forms.ComboBox cbSemis4;
        private System.Windows.Forms.ComboBox cbSemis3;
        private System.Windows.Forms.ComboBox cbSemis2;
        private System.Windows.Forms.ComboBox cbSemis1;
        private System.Windows.Forms.ComboBox cbCuartos8;
        private System.Windows.Forms.ComboBox cbCuartos6;
        private System.Windows.Forms.ComboBox cbCuartos7;
        private System.Windows.Forms.ComboBox cbCuartos5;
        private System.Windows.Forms.ComboBox cbCuartos4;
        private System.Windows.Forms.ComboBox cbCuartos2;
        private System.Windows.Forms.ComboBox cbCuartos3;
        private System.Windows.Forms.ComboBox cbCuartos1;
        private System.Windows.Forms.ComboBox cbOctavos16;
        private System.Windows.Forms.ComboBox cbOctavos15;
        private System.Windows.Forms.ComboBox cbOctavos14;
        private System.Windows.Forms.ComboBox cbOctavos13;
        private System.Windows.Forms.ComboBox cbOctavos12;
        private System.Windows.Forms.ComboBox cbOctavos11;
        private System.Windows.Forms.ComboBox cbOctavos10;
        private System.Windows.Forms.ComboBox cbOctavos9;
        private System.Windows.Forms.ComboBox cbOctavos8;
        private System.Windows.Forms.ComboBox cbOctavos7;
        private System.Windows.Forms.ComboBox cbOctavos6;
        private System.Windows.Forms.ComboBox cbOctavos5;
        private System.Windows.Forms.ComboBox cbOctavos4;
        private System.Windows.Forms.ComboBox cbOctavos3;
        private System.Windows.Forms.ComboBox cbOctavos2;
        private System.Windows.Forms.ComboBox cbOctavos1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
    }
}