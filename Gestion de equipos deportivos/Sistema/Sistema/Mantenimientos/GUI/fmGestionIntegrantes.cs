﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CacheManager;
using DataConexion;
using SessionManager;
namespace Mantenimientos.GUI
{
    public partial class fmGestionIntegrantes : Form
    {
        BindingSource _Integrantes = new BindingSource();
        private void CargarIntegrantes()
        {
            Cache Datos2 = new Cache();
            _Integrantes.DataSource = Datos2.Obtener(Cache.Consultas.TODOS_LOS_INTEGRANTES);
            FiltrarLocalmente();
        }
        private void FiltrarLocalmente()
        {
            if (txtBuscar.TextLength >0)
            {
                _Integrantes.Filter = "NombreIntegrante like '%" +txtBuscar.Text + "%'";
            }
            dtgvGestionEquipos.DataSource = _Integrantes;
            lblRegistrosEncontrados.Text = dtgvGestionEquipos.Rows.Count + "Registros Encontrados";
        }
        public fmGestionIntegrantes()
        {
            InitializeComponent();
        }

        private void fmGestionIntegrantes_Load(object sender, EventArgs e)
        {
            CargarIntegrantes();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmEdicionIntegrantes frm = new frmEdicionIntegrantes();
            frm.ShowDialog();
            CargarIntegrantes();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea modificar el registro?", "Pregunta", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                frmEdicionIntegrantes frm = new frmEdicionIntegrantes();
                frm.txtIDIntegrante.Text = dtgvGestionEquipos.CurrentRow.Cells["IDIntegrante"].Value.ToString();
                frm.txtNombreIntegrante.Text = dtgvGestionEquipos.CurrentRow.Cells["NombreIntegrante"].Value.ToString();
                frm.dtpFechaNacimiento.Text = dtgvGestionEquipos.CurrentRow.Cells["FechaNacimiento"].Value.ToString();
                frm.txtFechaCreacion.Text = dtgvGestionEquipos.CurrentRow.Cells["FechaCreacion"].Value.ToString();
                frm.Show();
                CargarIntegrantes();
            }
        }
    }
}
