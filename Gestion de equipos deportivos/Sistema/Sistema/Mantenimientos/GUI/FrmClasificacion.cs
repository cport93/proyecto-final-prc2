﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mantenimientos.GUI
{
    public partial class frmClasificacion : Form
    {
        DataTable resultado = new DataTable();
        
        DataTable rInsertar1 = new DataTable();
        DataTable rInsertar2 = new DataTable();
        DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
        StringBuilder Sentencia = new StringBuilder();

        public frmClasificacion()
        {
            InitializeComponent();
        }

        
        private void CargarOctavos()
        {

            var rng = new Random();
            var values = Enumerable.Range(0, 16).OrderBy(x => rng.Next()).ToArray();
            int Random1 = values[0];
            int Random2 = values[1];
            int Random3 = values[2];
            int Random4 = values[3];
            int Random5 = values[4];
            int Random6 = values[5];
            int Random7 = values[6];
            int Random8 = values[7];
            int Random9 = values[8];
            int Random10 = values[9];
            int Random11 = values[10];
            int Random12 = values[11];
            int Random13 = values[12];
            int Random14 = values[13];
            int Random15 = values[14];
            int Random16 = values[15];
            resultado = Operador.Consultar("SELECT NombreEquipo FROM sistema.equipos;");
            cbOctavos1.DataSource = resultado;
            cbOctavos1.DisplayMember = "NombreEquipo";
            cbOctavos1.SelectedIndex = Random1;
            cbOctavos1.BindingContext = new BindingContext();
            cbOctavos2.DataSource = resultado;
            cbOctavos2.DisplayMember = "NombreEquipo";
            cbOctavos2.ValueMember = "NombreEquipo";
            cbOctavos2.SelectedIndex = Random2;
            cbOctavos2.BindingContext = new BindingContext();
            cbOctavos3.DataSource = resultado;
            cbOctavos3.DisplayMember = "NombreEquipo";
            cbOctavos3.ValueMember = "NombreEquipo";
            cbOctavos3.SelectedIndex = Random3;
            cbOctavos3.BindingContext = new BindingContext();
            cbOctavos4.DataSource = resultado;
            cbOctavos4.DisplayMember = "NombreEquipo";
            cbOctavos4.SelectedIndex = Random4;
            cbOctavos4.BindingContext = new BindingContext();
            cbOctavos5.DataSource = resultado;
            cbOctavos5.DisplayMember = "NombreEquipo";
            cbOctavos5.SelectedIndex = Random5;
            cbOctavos5.BindingContext = new BindingContext();
            cbOctavos6.DataSource = resultado;
            cbOctavos6.DisplayMember = "NombreEquipo";
            cbOctavos6.SelectedIndex = Random6;
            cbOctavos6.BindingContext = new BindingContext();
            cbOctavos7.DataSource = resultado;
            cbOctavos7.DisplayMember = "NombreEquipo";
            cbOctavos7.SelectedIndex = Random7;
            cbOctavos7.BindingContext = new BindingContext();
            cbOctavos8.DataSource = resultado;
            cbOctavos8.DisplayMember = "NombreEquipo";
            cbOctavos8.SelectedIndex = Random8;
            cbOctavos8.BindingContext = new BindingContext();
            cbOctavos9.DataSource = resultado;
            cbOctavos9.DisplayMember = "NombreEquipo";
            cbOctavos9.SelectedIndex = Random9;
            cbOctavos9.BindingContext = new BindingContext();
            cbOctavos10.DataSource = resultado;
            cbOctavos10.DisplayMember = "NombreEquipo";
            cbOctavos10.SelectedIndex = Random10;
            cbOctavos10.BindingContext = new BindingContext();
            cbOctavos11.DataSource = resultado;
            cbOctavos11.DisplayMember = "NombreEquipo";
            cbOctavos11.SelectedIndex = Random11;
            cbOctavos11.BindingContext = new BindingContext();
            cbOctavos12.DataSource = resultado;
            cbOctavos12.DisplayMember = "NombreEquipo";
            cbOctavos12.SelectedIndex = Random12;
            cbOctavos12.BindingContext = new BindingContext();
            cbOctavos13.DataSource = resultado;
            cbOctavos13.DisplayMember = "NombreEquipo";
            cbOctavos13.SelectedIndex = Random13;
            cbOctavos13.BindingContext = new BindingContext();
            cbOctavos14.DataSource = resultado;
            cbOctavos14.DisplayMember = "NombreEquipo";
            cbOctavos13.SelectedIndex = Random14;
            cbOctavos14.BindingContext = new BindingContext();
            cbOctavos15.DataSource = resultado;
            cbOctavos15.DisplayMember = "NombreEquipo";
            cbOctavos15.SelectedIndex = Random15;
            cbOctavos15.BindingContext = new BindingContext();
            cbOctavos16.DataSource = resultado;
            cbOctavos16.DisplayMember = "NombreEquipo";
            cbOctavos16.SelectedIndex = Random16;
            cbOctavos16.BindingContext = new BindingContext();

        }
        //private void Refrescar()
        //{
        //    var row1 = (DataRowView)cbOctavos1.SelectedItem;
        //    var row2 = (DataRowView)cbOctavos2.SelectedItem;
        //    var row3 = (DataRowView)cbOctavos3.SelectedItem;
        //    var row4 = (DataRowView)cbOctavos4.SelectedItem;
        //    var row5 = (DataRowView)cbOctavos5.SelectedItem;
        //    var row6 = (DataRowView)cbOctavos6.SelectedItem;
        //    var row7 = (DataRowView)cbOctavos7.SelectedItem;
        //    var row8 = (DataRowView)cbOctavos8.SelectedItem;
        //    var row9 = (DataRowView)cbOctavos9.SelectedItem;
        //    var row10 = (DataRowView)cbOctavos10.SelectedItem;
        //    var row11 = (DataRowView)cbOctavos11.SelectedItem;
        //    var row12 = (DataRowView)cbOctavos12.SelectedItem;
        //    var row13 = (DataRowView)cbOctavos13.SelectedItem;
        //    var row14 = (DataRowView)cbOctavos14.SelectedItem;
        //    var row15 = (DataRowView)cbOctavos15.SelectedItem;
        //    var row16 = (DataRowView)cbOctavos16.SelectedItem;
        //    resultado = Operador.Consultar("SELECT NombreEquipo FROM sistema.equipos where NombreEquipo NOT IN ('" + row1[0] + "','" + row2[0] + "','" + row3[0] + "','" + row4[0] + "','" + row5[0] + "','" + row6[0] + "','" + row7[0] + "','" + row8[0] + "','" + row9[0] + "','" + row10[0] + "','" + row11[0] + "','" + row12[0] + "','" + row13[0] + "','" + row14[0] + "','" + row15[0] + "','" + row16[0] + "')");
        //    cbOctavos2.DataSource = resultado;
        //    cbOctavos2.DisplayMember = "NombreEquipo";
        //    cbOctavos2.ValueMember = "NombreEquipo";
        //    cbOctavos2.BindingContext = new BindingContext();
        //    cbOctavos3.DataSource = resultado;
        //    cbOctavos3.DisplayMember = "NombreEquipo";
        //    cbOctavos3.ValueMember = "NombreEquipo";
        //    cbOctavos3.BindingContext = new BindingContext();
        //    cbOctavos4.DataSource = resultado;
        //    cbOctavos4.DisplayMember = "NombreEquipo";
        //    cbOctavos4.ValueMember = "NombreEquipo";
        //    cbOctavos4.BindingContext = new BindingContext();
        //    cbOctavos5.DataSource = resultado;
        //    cbOctavos5.DisplayMember = "NombreEquipo";
        //    cbOctavos5.ValueMember = "NombreEquipo";
        //    cbOctavos5.BindingContext = new BindingContext();

        //}

        private void frmClasificacion_Load(object sender, EventArgs e)
        {
            CargarOctavos();
        }



        private void cbOctavos3_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void cbOctavos2_MouseClick(object sender, MouseEventArgs e)
        {
     
           
        }

        private void cbOctavos4_MouseClick(object sender, MouseEventArgs e)
        {
          
        }

        private void cbOctavos5_TextChanged(object sender, EventArgs e)
        {
            
        
            
        }

        private void frmClasificacion_MouseClick(object sender, MouseEventArgs e)
        {
   
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CargarOctavos();
        }

        private void txtOctavos1_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void txtOctavos2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable resultado2 = new DataTable();
      
                StringBuilder Sentencia2 = new StringBuilder();
                DataTable Partidosmax = new DataTable();
                //Del combobox se saco el dato que esta seleccionado para pode trabajarlo
                var row1 = (DataRowView)cbOctavos1.SelectedItem;
                string cuarto1 = row1[0].ToString();
                var row2 = (DataRowView)cbOctavos2.SelectedItem;
                string cuarto2 = row2[0].ToString();
                //Se hace la consulta para saber el ID del equipo ya que solo se conoce el nombre
                rInsertar1 = Operador.Consultar("Select IDEquipo from equipos where NombreEquipo = '"+cuarto1+"'");
                rInsertar2 = Operador.Consultar("Select IDEquipo from equipos where NombreEquipo = '" + cuarto2 + "'");
                //Se hicieron 2 busquedas ya que no se logro sacar las 2 lineas de una sola consulta
                DataRow linea1 = rInsertar1.Rows[0];
                DataRow linea2 = rInsertar2.Rows[0];
                //Se obtienen los valores de la consulta y se guardan en un String
                String valor1 = linea1[0].ToString();
                String valor2 = linea2[0].ToString();
                //Se deshabilitan los textos para que no pueda modificarse
                txtOctavos1.Enabled = false;
                txtOctavos2.Enabled = false;
                int octavo1 = Int32.Parse(txtOctavos1.Text);
                int octavo2 = Int32.Parse(txtOctavos2.Text);
                //Se hace la insersión a partidos como partido jugado
                Sentencia.Append("insert into partidos(IDEquipo1, IDEquipo2,GolesEquipo1, GolesEquipo2) values('" + valor1 + "','" + valor2 + "','" + octavo1 + "','" + octavo2 + "')");
                Operador.Insertar(Sentencia.ToString());
                if (octavo1 > octavo2)
                {
                    //Resultado 2 sirve para llenar el datasource de los combobox
                    resultado2 = Operador.Consultar("SELECT e.NombreEquipo FROM sistema.partidos p inner join equipos e on p.IDEquipo1 = e.IDEquipo where p.IDEquipo1 = " + valor1 + "");
                    //El DataTable partidosmax sirve para saber cual es el ultimo partido jugado, lo que significa que dentro de ese partido se gano
                    Partidosmax = Operador.Consultar("SELECT max(IDPartido) as IDPartido FROM sistema.partidos;");
                    ///Esta linea se genera siempre, no se le pone ningun numeral
                    /////Se hizo un gran procedimiento de datarow to string to int ya que el valor que nos pide el ID partido es INT
                    DataRow lineamax = Partidosmax.Rows[0];
                    String valormax = lineamax[0].ToString();
                    int intvalormax = Int32.Parse(valormax);
                    //Insertando al Ganador 
                    Sentencia2.Append("insert into resultados(IDPartido, Etapa, Ganador) values ("+intvalormax+",'octavos','"+cuarto1+"')");
                    Operador.Insertar(Sentencia2.ToString());
                    cbCuartos1.DataSource = resultado2;
                    cbCuartos1.ValueMember = ("NombreEquipo");
                    cbCuartos1.SelectedValue = (cuarto1);
                    cbCuartos1.BindingContext = new BindingContext();
                }
                else
                {
                    //Resultado 2 sirve para llenar el datasource de los combobox
                    resultado2 = Operador.Consultar("SELECT e.NombreEquipo FROM sistema.partidos p inner join equipos e on p.IDEquipo2 = e.IDEquipo where p.IDEquipo2 = " +valor2+"");
                    //El DataTable partidosmax sirve para saber cual es el ultimo partido jugado, lo que significa que dentro de ese partido se gano
                    Partidosmax = Operador.Consultar("SELECT max(IDPartido) as IDPartido FROM sistema.partidos;");
                    ///Esta linea se genera siempre, no se le pone ningun numeral
                    /////Se hizo un gran procedimiento de datarow to string to int ya que el valor que nos pide el ID partido es INT
                    DataRow lineamax = Partidosmax.Rows[0];
                    String valormax = lineamax[0].ToString();
                    int intvalormax = Int32.Parse(valormax);
                    //Insertando al ganador
                    Sentencia2.Append("insert into resultados(IDPartido, Etapa, Ganador) values (" + intvalormax + ",'octavos','" + cuarto2 + "')");
                    Operador.Insertar(Sentencia2.ToString());
                    cbCuartos1.DataSource = resultado2;
                    cbCuartos1.ValueMember = ("NombreEquipo");
                    cbCuartos1.SelectedValue = (cuarto2);
                    cbCuartos1.BindingContext = new BindingContext();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Por favor ingrese un valor correcto"+ex.Message);
                txtOctavos1.Enabled = true;
                txtOctavos2.Enabled = true;
            }

        }

        private void txtOctavos4_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtOctavos3.Enabled = false;
                txtOctavos4.Enabled = false;
                int octavo3 = Int32.Parse(txtOctavos3.Text);
                int octavo4 = Int32.Parse(txtOctavos4.Text);
                if (octavo3 > octavo4)
                {
                    var row3 = (DataRowView)cbOctavos3.SelectedItem;
                    string cuarto2 = row3[0].ToString();    
                    cbCuartos2.DataSource = resultado;
                    cbCuartos2.ValueMember = ("NombreEquipo");
                    cbCuartos2.SelectedValue = (cuarto2);
                    cbCuartos2.BindingContext = new BindingContext();
                }
                else
                {
                    var row4 = (DataRowView)cbOctavos4.SelectedItem;
                    string cuarto2 = row4[0].ToString();
                    cbCuartos2.DataSource = resultado;
                    cbCuartos2.ValueMember = ("NombreEquipo");
                    cbCuartos2.SelectedValue = (cuarto2);
                    cbCuartos2.BindingContext = new BindingContext();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Por favor ingrese un valor correcto" + ex.Message);
                txtOctavos3.Enabled = true;
                txtOctavos4.Enabled = true;
            }
        }

        private void txtOctavos6_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtOctavos5.Enabled = false;
                txtOctavos6.Enabled = false;
                int octavo5 = Int32.Parse(txtOctavos5.Text);
                int octavo6 = Int32.Parse(txtOctavos6.Text);
                if (octavo5 > octavo6)
                {
                    var row5 = (DataRowView)cbOctavos5.SelectedItem;
                    string cuarto3 = row5[0].ToString();
                    cbCuartos3.DataSource = resultado;
                    cbCuartos3.ValueMember = ("NombreEquipo");
                    cbCuartos3.SelectedValue = (cuarto3);
                    cbCuartos3.BindingContext = new BindingContext();
                }
                else
                {
                    var row6 = (DataRowView)cbOctavos6.SelectedItem;
                    string cuarto3 = row6[0].ToString();
                    cbCuartos3.DataSource = resultado;
                    cbCuartos3.ValueMember = ("NombreEquipo");
                    cbCuartos3.SelectedValue = (cuarto3);
                    cbCuartos3.BindingContext = new BindingContext();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Por favor ingrese un valor correcto" + ex.Message);
                txtOctavos5.Enabled = true;
                txtOctavos6.Enabled = true;
            }
        }

        private void txtOctavos8_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtOctavos7.Enabled = false;
                txtOctavos8.Enabled = false;
                int octavo7 = Int32.Parse(txtOctavos7.Text);
                int octavo8 = Int32.Parse(txtOctavos8.Text);
                if (octavo7 > octavo8)
                {
                    var row = (DataRowView)cbOctavos7.SelectedItem;
                    string cuarto4 = row[0].ToString();
                    cbCuartos4.DataSource = resultado;
                    cbCuartos4.ValueMember = ("NombreEquipo");
                    cbCuartos4.SelectedValue = (cuarto4);
                    cbCuartos4.BindingContext = new BindingContext();
                }
                else
                {
                    var row = (DataRowView)cbOctavos8.SelectedItem;
                    string cuarto4 = row[0].ToString();
                    cbCuartos4.DataSource = resultado;
                    cbCuartos4.ValueMember = ("NombreEquipo");
                    cbCuartos4.SelectedValue = (cuarto4);
                    cbCuartos4.BindingContext = new BindingContext();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Por favor ingrese un valor correcto" + ex.Message);
                txtOctavos7.Enabled = true;
                txtOctavos8.Enabled = true;
            }
        }

        private void txtOctavos10_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtOctavos9.Enabled = false;
                txtOctavos10.Enabled = false;
                int octavo9 = Int32.Parse(txtOctavos9.Text);
                int octavo10 = Int32.Parse(txtOctavos10.Text);
                if (octavo9 > octavo10)
                {
                    var row = (DataRowView)cbOctavos9.SelectedItem;
                    string cuarto5 = row[0].ToString();
                    cbCuartos5.DataSource = resultado;
                    cbCuartos5.ValueMember = ("NombreEquipo");
                    cbCuartos5.SelectedValue = (cuarto5);
                    cbCuartos5.BindingContext = new BindingContext();
                }
                else
                {
                    var row = (DataRowView)cbOctavos10.SelectedItem;
                    string cuarto5 = row[0].ToString();
                    cbCuartos5.DataSource = resultado;
                    cbCuartos5.ValueMember = ("NombreEquipo");
                    cbCuartos5.SelectedValue = (cuarto5);
                    cbCuartos5.BindingContext = new BindingContext();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Por favor ingrese un valor correcto" + ex.Message);
                txtOctavos9.Enabled = true;
                txtOctavos10.Enabled = true;
            }

        }

        private void txtOctavos12_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtOctavos11.Enabled = false;
                txtOctavos12.Enabled = false;
                int octavo11 = Int32.Parse(txtOctavos11.Text);
                int octavo12 = Int32.Parse(txtOctavos12.Text);
                if (octavo11 > octavo12)
                {
                    var row = (DataRowView)cbOctavos11.SelectedItem;
                    string cuarto6 = row[0].ToString();
                    cbCuartos6.DataSource = resultado;
                    cbCuartos6.ValueMember = ("NombreEquipo");
                    cbCuartos6.SelectedValue = (cuarto6);
                    cbCuartos6.BindingContext = new BindingContext();

                }
                else
                {
                    var row = (DataRowView)cbOctavos12.SelectedItem;
                    string cuarto6 = row[0].ToString();
                    cbCuartos6.DataSource = resultado;
                    cbCuartos6.ValueMember = ("NombreEquipo");
                    cbCuartos6.SelectedValue = (cuarto6);
                    cbCuartos6.BindingContext = new BindingContext();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Por favor ingrese un valor correcto" + ex.Message);
                txtOctavos11.Enabled = true;
                txtOctavos12.Enabled = true;
            }
        }

        private void txtOctavos14_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtOctavos13.Enabled = false;
                txtOctavos14.Enabled = false;
                int octavo13 = Int32.Parse(txtOctavos13.Text);
                int octavo14 = Int32.Parse(txtOctavos14.Text);
                if (octavo13 > octavo14)
                {
                    var row = (DataRowView)cbOctavos13.SelectedItem;
                    string cuarto7 = row[0].ToString();
                    cbCuartos7.DataSource = resultado;
                    cbCuartos7.ValueMember = ("NombreEquipo");
                    cbCuartos7.SelectedValue = (cuarto7);
                    cbCuartos7.BindingContext = new BindingContext();
                }
                else
                {
                    var row = (DataRowView)cbOctavos14.SelectedItem;
                    string cuarto7 = row[0].ToString();
                    cbCuartos7.DataSource = resultado;
                    cbCuartos7.ValueMember = ("NombreEquipo");
                    cbCuartos7.SelectedValue = (cuarto7);
                    cbCuartos7.BindingContext = new BindingContext();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Por favor ingrese un valor correcto" + ex.Message);
                txtOctavos13.Enabled = true;
                txtOctavos14.Enabled = true;
            }
        }

        private void txtOctavos16_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtOctavos15.Enabled = false;
                txtOctavos16.Enabled = false;
                int octavo15 = Int32.Parse(txtOctavos15.Text);
                int octavo16 = Int32.Parse(txtOctavos16.Text);
                if (octavo15 > octavo16)
                {
                    var row = (DataRowView)cbOctavos15.SelectedItem;
                    string cuarto8 = row[0].ToString();
                    cbCuartos8.DataSource = resultado;
                    cbCuartos8.ValueMember = ("NombreEquipo");
                    cbCuartos8.SelectedValue = (cuarto8);
                    cbCuartos8.BindingContext = new BindingContext();
                }
                else
                {
                    var row = (DataRowView)cbOctavos16.SelectedItem;
                    string cuarto8 = row[0].ToString();
                    cbCuartos8.DataSource = resultado;
                    cbCuartos8.ValueMember = ("NombreEquipo");
                    cbCuartos8.SelectedValue = (cuarto8);
                    cbCuartos8.BindingContext = new BindingContext();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Por favor ingrese un valor correcto" + ex.Message);
                txtOctavos15.Enabled = true;
                txtOctavos16.Enabled = true;
            }
        }

    }
}
