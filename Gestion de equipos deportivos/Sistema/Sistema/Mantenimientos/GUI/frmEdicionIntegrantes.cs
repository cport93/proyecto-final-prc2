﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mantenimientos.GUI
{
    public partial class frmEdicionIntegrantes : Form
    {
        public frmEdicionIntegrantes()
        {
            InitializeComponent();
        }

        private void Procesar()
        {
            //Creamos el objeto grupo vacio
            CLS.Integrante objIntegrante = new CLS.Integrante();
            //Se sincroniza el objeto
            objIntegrante.IDIntegrante = txtIDIntegrante.Text;
            objIntegrante.NombreIntegrante = txtNombreIntegrante.Text;
            objIntegrante.IDEquipo = cbIDGrupo.Text;
            objIntegrante.FechaCreacion = txtFechaCreacion.Text;
            objIntegrante.FechaNacimiento = dtpFechaNacimiento.Text;
        
            if (txtIDIntegrante.TextLength > 0)
            {
               
                if (objIntegrante.Actualizar())
                {
                    MessageBox.Show("Registro actualizado", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    Close();
                }
                else
                {
                    MessageBox.Show("Registro no fue actualizado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("Registro insertado", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                objIntegrante.Guardar();
                Close();
            }
        }
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void frmEdicionIntegrantes_Load(object sender, EventArgs e)
        {
            DataTable resultado = new DataTable();
            DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
            resultado = Operador.Consultar("SELECT IDEquipo FROM sistema.equipos;");
            cbIDGrupo.DataSource = resultado;
            cbIDGrupo.DisplayMember = "IDEquipo";
            this.cbIDGrupo.DropDownStyle = ComboBoxStyle.DropDownList;  
            

            

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Procesar();
        }
    }
}
