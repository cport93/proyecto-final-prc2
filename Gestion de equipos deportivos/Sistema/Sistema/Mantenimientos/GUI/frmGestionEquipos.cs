﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataConexion;
using CacheManager;
using SessionManager;
namespace Mantenimientos.GUI
{
    public partial class frmGestionEquipos : Form
    {
        BindingSource _Equipos = new BindingSource();
        private void CargarGrupos()
        {
            Cache Datos = new Cache();
            _Equipos.DataSource = Datos.Obtener(Cache.Consultas.EQUIPOS_INSCRITOS);
            FiltrarLocalmente();

        }
        private void FiltrarLocalmente()
        {
            if (txtBuscar.TextLength > 0)
            {
                _Equipos.Filter = "NombreEquipo LIKE '%" + txtBuscar.Text + "%'";
            }
            else
            {
                _Equipos.RemoveFilter();
            }
            dtgvGestionEquipos.DataSource = _Equipos;
            lblRegistrosEncontrados.Text = dtgvGestionEquipos.Rows.Count + " Registros encontrados";
        }
        public frmGestionEquipos()
        {
            InitializeComponent();
        }

        private void frmGestionEquipos_Load(object sender, EventArgs e)
        {
            CargarGrupos();
        }


        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmEdicionEquipos frm = new frmEdicionEquipos();
            frm.ShowDialog();
            CargarGrupos();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea modificar el registro?", "Pregunta", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                frmEdicionEquipos frm = new frmEdicionEquipos();
                frm.txtIDEquipo.Text = dtgvGestionEquipos.CurrentRow.Cells["IDEquipos"].Value.ToString();
                frm.txtNombreEquipo.Text = dtgvGestionEquipos.CurrentRow.Cells["NombreEquipo"].Value.ToString();
                frm.txtFechaCreacion.Text = dtgvGestionEquipos.CurrentRow.Cells["FechaCreacion"].Value.ToString();
                frm.Show();
                CargarGrupos();
            }
        }

        private void dtgvGestionEquipos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea eliminar el registro?", "Aviso", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                CLS.Equipo objEquipo = new CLS.Equipo();
                objEquipo.IDEquipo = dtgvGestionEquipos.CurrentRow.Cells["IDEquipos"].Value.ToString();
                if (objEquipo.Eliminar())
                {
                    MessageBox.Show("Registro Eliminado");
                    CargarGrupos();

                }
                else
                {
                    MessageBox.Show("El registro no fue elmiinado");
                }
            }
        }
    }
}
