﻿namespace Mantenimientos.GUI
{
    partial class frmEdicionIntegrantes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.txtNombreIntegrante = new System.Windows.Forms.TextBox();
            this.Grupo = new System.Windows.Forms.Label();
            this.txtIDIntegrante = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbIDGrupo = new System.Windows.Forms.ComboBox();
            this.txtFechaCreacion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Fecha de Nacimiento";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(228, 233);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 19;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(131, 233);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 18;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtNombreIntegrante
            // 
            this.txtNombreIntegrante.Location = new System.Drawing.Point(142, 70);
            this.txtNombreIntegrante.Name = "txtNombreIntegrante";
            this.txtNombreIntegrante.Size = new System.Drawing.Size(194, 20);
            this.txtNombreIntegrante.TabIndex = 17;
            // 
            // Grupo
            // 
            this.Grupo.AutoSize = true;
            this.Grupo.Location = new System.Drawing.Point(29, 77);
            this.Grupo.Name = "Grupo";
            this.Grupo.Size = new System.Drawing.Size(95, 13);
            this.Grupo.TabIndex = 16;
            this.Grupo.Text = "Nombre Integrante";
            // 
            // txtIDIntegrante
            // 
            this.txtIDIntegrante.Enabled = false;
            this.txtIDIntegrante.Location = new System.Drawing.Point(142, 32);
            this.txtIDIntegrante.Name = "txtIDIntegrante";
            this.txtIDIntegrante.Size = new System.Drawing.Size(194, 20);
            this.txtIDIntegrante.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "ID Integrante";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 26);
            this.label3.TabIndex = 22;
            this.label3.Text = "Grupo al que pertenece\r\n(Ingrese el ID)";
            // 
            // cbIDGrupo
            // 
            this.cbIDGrupo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbIDGrupo.FormattingEnabled = true;
            this.cbIDGrupo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbIDGrupo.Location = new System.Drawing.Point(145, 146);
            this.cbIDGrupo.Name = "cbIDGrupo";
            this.cbIDGrupo.Size = new System.Drawing.Size(121, 21);
            this.cbIDGrupo.TabIndex = 23;
            // 
            // txtFechaCreacion
            // 
            this.txtFechaCreacion.Enabled = false;
            this.txtFechaCreacion.Location = new System.Drawing.Point(142, 184);
            this.txtFechaCreacion.Name = "txtFechaCreacion";
            this.txtFechaCreacion.Size = new System.Drawing.Size(194, 20);
            this.txtFechaCreacion.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Fecha de Creacion";
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(145, 104);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaNacimiento.TabIndex = 27;
            // 
            // frmEdicionIntegrantes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 356);
            this.Controls.Add(this.dtpFechaNacimiento);
            this.Controls.Add(this.txtFechaCreacion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbIDGrupo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtNombreIntegrante);
            this.Controls.Add(this.Grupo);
            this.Controls.Add(this.txtIDIntegrante);
            this.Controls.Add(this.label1);
            this.Name = "frmEdicionIntegrantes";
            this.Text = "frmEdicionIntegrantescs";
            this.Load += new System.EventHandler(this.frmEdicionIntegrantes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        public System.Windows.Forms.TextBox txtNombreIntegrante;
        private System.Windows.Forms.Label Grupo;
        public System.Windows.Forms.TextBox txtIDIntegrante;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtFechaCreacion;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox cbIDGrupo;
        public System.Windows.Forms.DateTimePicker dtpFechaNacimiento;

    }
}