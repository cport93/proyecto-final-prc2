﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mantenimientos.GUI
{
    public partial class frmEdicionGrupos : Form
    {
        public frmEdicionGrupos()
        {
            InitializeComponent();
        }

        private void Procesar()
        {
            //Creamos el objeto grupo vacio
            CLS.Grupo objGrupo = new CLS.Grupo();
            //Se sincroniza el objeto
            objGrupo.IDGrupo = txtIDGrupo.Text;
            objGrupo.NombreGrupo = txtGrupo.Text;
            {
                if (txtIDGrupo.TextLength >0)
                {
                    //Estamos modificando
                    if (objGrupo.Actualizar())
                    {
                        MessageBox.Show("Registro actualizado", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Registro no fue actualizado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else 
                {
                    MessageBox.Show("Registro insertado", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    objGrupo.Guardar();
                    Close();
                }
            }

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

            Procesar();
            
        }

        private void EdicionGrupos_Load(object sender, EventArgs e)
        {

        }
    }
}
