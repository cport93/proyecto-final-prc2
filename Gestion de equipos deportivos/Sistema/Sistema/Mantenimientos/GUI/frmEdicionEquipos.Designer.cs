﻿namespace Mantenimientos.GUI
{
    partial class frmEdicionEquipos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.txtNombreEquipo = new System.Windows.Forms.TextBox();
            this.Grupo = new System.Windows.Forms.Label();
            this.txtIDEquipo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFechaCreacion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(237, 135);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 11;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(140, 135);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 10;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtNombreEquipo
            // 
            this.txtNombreEquipo.Location = new System.Drawing.Point(115, 60);
            this.txtNombreEquipo.Name = "txtNombreEquipo";
            this.txtNombreEquipo.Size = new System.Drawing.Size(194, 20);
            this.txtNombreEquipo.TabIndex = 9;
            this.txtNombreEquipo.TextChanged += new System.EventHandler(this.txtGrupo_TextChanged);
            // 
            // Grupo
            // 
            this.Grupo.AutoSize = true;
            this.Grupo.Location = new System.Drawing.Point(25, 67);
            this.Grupo.Name = "Grupo";
            this.Grupo.Size = new System.Drawing.Size(80, 13);
            this.Grupo.TabIndex = 8;
            this.Grupo.Text = "Nombre Equipo";
            // 
            // txtIDEquipo
            // 
            this.txtIDEquipo.Enabled = false;
            this.txtIDEquipo.Location = new System.Drawing.Point(115, 22);
            this.txtIDEquipo.Name = "txtIDEquipo";
            this.txtIDEquipo.Size = new System.Drawing.Size(194, 20);
            this.txtIDEquipo.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "ID Equipo";
            // 
            // txtFechaCreacion
            // 
            this.txtFechaCreacion.Enabled = false;
            this.txtFechaCreacion.Location = new System.Drawing.Point(115, 93);
            this.txtFechaCreacion.Multiline = true;
            this.txtFechaCreacion.Name = "txtFechaCreacion";
            this.txtFechaCreacion.Size = new System.Drawing.Size(132, 20);
            this.txtFechaCreacion.TabIndex = 13;
            this.txtFechaCreacion.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Fecha de Creacion";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // frmEdicionEquipos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 170);
            this.Controls.Add(this.txtFechaCreacion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtNombreEquipo);
            this.Controls.Add(this.Grupo);
            this.Controls.Add(this.txtIDEquipo);
            this.Controls.Add(this.label1);
            this.Name = "frmEdicionEquipos";
            this.Text = "frmEdicionEquipos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        public System.Windows.Forms.TextBox txtNombreEquipo;
        private System.Windows.Forms.Label Grupo;
        public System.Windows.Forms.TextBox txtIDEquipo;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtFechaCreacion;
        private System.Windows.Forms.Label label2;
    }
}