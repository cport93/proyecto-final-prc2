﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimientos.CLS
{
    class Integrante
    {
        String _IDIntegrante;
        String _NombreIntegrante;
        String _FechaNacimiento;
        String _IDEquipo;
        String _FechaCreacion;

      
        public String IDIntegrante
        {
            get { return _IDIntegrante; }
            set { _IDIntegrante = value; }
        }
        public String NombreIntegrante
        {
            get { return _NombreIntegrante; }
            set { _NombreIntegrante = value; }
        }
        public String FechaNacimiento
        {
            get { return _FechaNacimiento; }
            set { _FechaNacimiento = value; }
        }
        public String IDEquipo
        {
            get { return _IDEquipo; }
            set { _IDEquipo = value; }
        }
        public String FechaCreacion
        {
            get { return _FechaCreacion; }
            set { _FechaCreacion = value; }
        }

        public Boolean Guardar()
        {
            Boolean Guardado = false;
            DataConexion.DBOperacion operador = new DataConexion.DBOperacion();
            StringBuilder Sentencia = new StringBuilder();

            Sentencia.Append("insert into integrantes(NombreIntegrante, FechaNacimiento, IDEquipo) values('"+_NombreIntegrante+"',STR_TO_DATE('"+_FechaNacimiento+"','%d/%m/%Y')," + _IDEquipo + ")");
            try 
            {
                if (operador.Insertar(Sentencia.ToString()) >0)   
                {
                    Guardado = true ;
                }
                else
                {
                    Guardado = false;
                }

            }
            catch
            {
                Guardado = false;
            }  
            return Guardado;
        }

        public Boolean Actualizar()
        {
            Boolean Actualizado = false;
            DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
            StringBuilder Sentencia = new StringBuilder();
            //Construyendo la sentencia
            Sentencia.Append("UPDATE integrantes SET NombreIntegrante ='" + _NombreIntegrante + "',FechaNacimiento = STR_TO_DATE('"+_FechaNacimiento+"','%d/%m/%Y'), IDEquipo = " + _IDEquipo + " WHERE IDIntegrante = " + IDIntegrante + ";");
            try
            {
                if (Operador.Actualizar(Sentencia.ToString()) > 0)
                {
                    Actualizado = true;
                }
                else
                {
                    Actualizado = false;
                }

            }
            catch
            {
                Actualizado = false;
            }

            return Actualizado;
        }

    }
}
