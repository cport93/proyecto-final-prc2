﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimientos.CLS
{
    class Usuario
    {
        String _IDUsuario;
        String _NombreUsuario;
        String _Credencial;
        String _IDEmpleado;
        String _IDGrupo;
        String _FechaCreacion;

        public String FechaCreacion
        {
            get { return _FechaCreacion; }
            set { _FechaCreacion = value; }
        }

        public String IDGrupo
        {
            get { return _IDGrupo; }
            set { _IDGrupo = value; }
        }

        public String IDEmpleado
        {
            get { return _IDEmpleado; }
            set { _IDEmpleado = value; }
        }

        public String Credencial
        {
            get { return _Credencial; }
            set { _Credencial = value; }
        }

        public String NombreUsuario
        {
            get { return _NombreUsuario; }
            set { _NombreUsuario = value; }
        }

        public String IDUsuario
        {
            get { return _IDUsuario; }
            set { _IDUsuario = value; }
        }


        public Boolean Guardar()
        {
            Boolean Guardado = false;
            DataConexion.DBOperacion operador = new DataConexion.DBOperacion();
            StringBuilder Sentencia = new StringBuilder();

            Sentencia.Append("insert into usuarios(Usuario, Credencial, IDEmpleado, IDGrupo) values('" + _NombreUsuario + "',md5("+_Credencial+"),"+_IDEmpleado+","+_IDGrupo+")");
            try
            {
                if (operador.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }

            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean Actualizar()
        {
            Boolean Actualizado = false;
            DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
            StringBuilder Sentencia = new StringBuilder();
            //Construyendo la sentencia
            Sentencia.Append("UPDATE usuarios SET Usuario ='" +_NombreUsuario+ "',Credencial = MD5('"+_Credencial+"'), IDEmpleado ="+ _IDEmpleado +", IDGrupo = "+_IDGrupo+" WHERE IDUsuario = " + _IDUsuario+ ";");
            try
            {
                if (Operador.Actualizar(Sentencia.ToString()) > 0)
                {
                    Actualizado = true;
                }
                else
                {
                    Actualizado = false;
                }

            }
            catch
            {
                Actualizado = false;
            }

            return Actualizado;
        }
    }
}
