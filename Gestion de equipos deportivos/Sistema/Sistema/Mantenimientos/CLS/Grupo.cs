﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using DataConexion;
namespace Mantenimientos.CLS
{
    class Grupo
    {
        
        //ATRIBUTOS
        /// <summary>
        /// POR CADA COLUMAN DE LA TABLA GRUPOS SE AGREGA UN ATRIBUTO
        /// </summary>
        String _IDGrupo;
        String _Grupo;
        //PROPIEDADES
        public String NombreGrupo
        {
            get { return _Grupo; }
            set { _Grupo = value; }
        }

        public String IDGrupo
        {
            get { return _IDGrupo; }
            set { _IDGrupo = value; }
        }
        //ACCIONES
        public Boolean Guardar()
        {
            
            
            Boolean Guardado = false;
            DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
            
            StringBuilder Sentencia = new StringBuilder();
            StringBuilder Sentencia2 = new StringBuilder();

            //Construyendo la sentencia
        
            Sentencia.Append("INSERT INTO grupos(Grupo) Values ('" + _Grupo +"');");
            try
            {
                if (Operador.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }

            }
            catch 
            {
                Guardado = false;
            }
            
            return Guardado;
        }

        public Boolean Actualizar()
        {
            Boolean Actualizado = false;
            DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
            StringBuilder Sentencia = new StringBuilder();
            //Construyendo la sentencia
            Sentencia.Append("UPDATE grupos SET grupo = '" + _Grupo + "' WHERE IDGrupos = "+_IDGrupo+";");
            try
            {
                if (Operador.Actualizar(Sentencia.ToString()) > 0)
                {
                    Actualizado = true;
                }
                else
                {
                    Actualizado = false;
                }

            }
            catch
            {
                Actualizado = false;
            }

            return Actualizado;
        }

        public Boolean Eliminar()
        {
            Boolean Eliminado = false;
            DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
            StringBuilder Sentencia = new StringBuilder();
            //Construyendo la sentencia
            Sentencia.Append("Delete from grupos where IDGrupos="+ _IDGrupo + ";");
            try
            {
                if (Operador.Actualizar(Sentencia.ToString()) > 0)
                {
                    Eliminado = true;
                }
                else
                {
                    Eliminado = false;
                }

            }
            catch
            {
                Eliminado = false;
            }

            return Eliminado;
        }

    }
}
