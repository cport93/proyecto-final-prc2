﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimientos.CLS
{
    class Equipo
    {
        String _IDEquipo;
        String _NombreEquipo;
        String _FechaCreacion;

        public String IDEquipo
        {
            get { return _IDEquipo; }
            set { _IDEquipo = value; }
        }
        public String NombreEquipo
        {
            get { return _NombreEquipo; }
            set { _NombreEquipo = value; }
        }
        public String FechaCreacion
        {
            get { return _FechaCreacion; }
            set { _FechaCreacion = value; }
        }

                public Boolean Guardar()
                {
                    Boolean Guardado = false;
                    DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
                    StringBuilder Sentencia = new StringBuilder();
                    //Construyendo la sentencia
                    Sentencia.Append("INSERT INTO equipos(NombreEquipo) Values ('" + _NombreEquipo + "');");
                    try
                    {
                        if (Operador.Insertar(Sentencia.ToString()) > 0)
                        {
                            Guardado = true;
                        }
                        else
                        {
                            Guardado = false;
                        }

                    }
                    catch
                    {
                        Guardado = false;
                    }

                    return Guardado;
                }

        public Boolean Actualizar()
        {
            Boolean Actualizado = false;
            DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
            StringBuilder Sentencia = new StringBuilder();
            //Construyendo la sentencia
            Sentencia.Append("UPDATE equipos SET NombreEquipo = '" + _NombreEquipo + "' WHERE IDEquipo = " + _IDEquipo + ";");
            try
            {
                if (Operador.Actualizar(Sentencia.ToString()) > 0)
                {
                    Actualizado = true;
                }
                else
                {
                    Actualizado = false;
                }

            }
            catch
            {
                Actualizado = false;
            }

            return Actualizado;
        }

        public Boolean Eliminar()
        {
            Boolean Eliminado = false;
            DataConexion.DBOperacion Operador = new DataConexion.DBOperacion();
            StringBuilder Sentencia = new StringBuilder();
            //Construyendo la sentencia
            Sentencia.Append("Delete from equipos where IDEquipo=" + _IDEquipo + ";");
            try
            {
                if (Operador.Actualizar(Sentencia.ToString()) > 0)
                {
                    Eliminado = true;
                }
                else
                {
                    Eliminado = false;
                }

            }
            catch
            {
                Eliminado = false;
            }

            return Eliminado;
        }
    }
}
