﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SessionManager
{
    public class Sesion
    {
        private static volatile Sesion _Instancia = null;
        public static Sesion Instancia
        {
            get 
            {
                Object bloqueador = new Object();
                if(_Instancia==null)
                {
                    lock(bloqueador)
                    {
                        if(_Instancia==null)
                        {
                            _Instancia = new Sesion();
                        }
                    }
                }
                return _Instancia;
            }
        }

        String _Usuario;
        String _IDGrupo;
        String _Rol;

        
        public String IDGrupo
        {
            get { return _IDGrupo; }
            set { _IDGrupo = value; }
        }
        DataTable PermisosAsignados = new DataTable();

        public String Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }
        public String Rol
        {
            get { return _Rol; }
            set { _Rol = value; }
        }

        public void ObtenerPrivilegios()
        {
            try
            {
                CacheManager.Cache Consultor = new CacheManager.Cache();
                Consultor.Filtro = _IDGrupo;
                PermisosAsignados = Consultor.Obtener(CacheManager.Cache.Consultas.PRIVILEGIOS_ASIGNADOS_SEGUN_IDGRUPO);
            }
            catch
            {
                PermisosAsignados = new DataTable();
            }
        }
        public Boolean ComprobarPermiso (Int32 pIDPrivilegio)
        {
            Boolean Encontrado = false;
            Int32 valor;
            foreach (DataRow fila in PermisosAsignados.Rows)
            {
                valor = Convert.ToInt32 (fila["IDPrivilegio"].ToString());
                if(valor == pIDPrivilegio)
                {
                    Encontrado = true;
                        break;
                }
            }
            return Encontrado;
        }

        private Sesion()
        {

        }
    }
}
