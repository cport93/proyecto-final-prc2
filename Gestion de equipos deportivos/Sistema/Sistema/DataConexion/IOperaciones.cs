﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataConexion
{
    interface IOperaciones
    {
        Int32 Insertar(String pSentencia);
        Int32 Actualizar(String pSentencia);
        Int32 Eliminar(String pSentencia);
        DataTable Consultar(String pConsulta);
    }
}
