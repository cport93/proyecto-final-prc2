﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
namespace DataConexion
{
    public class DBOperacion:DBConexion,IOperaciones
    {
        private Int32 EjecutarSentencia(String pSentencia)
        {
            Int32 FilasAfectadas = 0;
            try
            {
                if (base.Conectar())
                {
                    MySqlCommand comando = new MySqlCommand();
                    comando.CommandText = pSentencia;
                    comando.Connection = base.CONEXION;
                    FilasAfectadas = comando.ExecuteNonQuery();
                }
                else
                {
                    FilasAfectadas = 0;
                }
            }
            catch
            {
                FilasAfectadas = 0;
            }
            return FilasAfectadas;
        }
        private DataTable EjecutarConsulta (String pConsulta)
        {
            DataTable Resultado = new DataTable();
            try
            {
                if (base.Conectar())
                {
                    MySqlCommand comando = new MySqlCommand();
                    MySqlDataAdapter Adaptador = new MySqlDataAdapter();
                    comando.Connection = base.CONEXION;
                    comando.CommandText = pConsulta;
                    Adaptador.SelectCommand = comando;
                    Adaptador.Fill(Resultado);
                }
            }
            catch (MySqlException Ex)
            {
                Console.WriteLine("error" + Ex.GetBaseException());
            }            
            return Resultado;
        }


        public int Insertar(string pSentencia)
        {
            return EjecutarSentencia(pSentencia);
        }

        public int Actualizar(string pSentencia)
        {
            return EjecutarSentencia(pSentencia);
        }

        public int Eliminar(string pSentencia)
        {
            return EjecutarSentencia(pSentencia);
        }

        public DataTable Consultar(string pConsulta)
        {
            return EjecutarConsulta(pConsulta);
        }
    }
}
