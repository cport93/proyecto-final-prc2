﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
namespace DataConexion
{
    public class DBConexion
    {
        private MySqlConnection _CONEXION = new MySqlConnection();

        public MySqlConnection CONEXION
        {
            get { return _CONEXION; }
            set { _CONEXION = value; }
        }
        public Boolean Conectar()
        {
            Boolean Conectado = false;
            //CONFIGURAMOS LA CADENA DE CONEXION
            String CadenaConexion = "Server=localhost;Port=3306;Database=sistema;Uid=root;Pwd=admin;";
            //ESTABLECEMOS LA CADENA DE CONEXION A UTILIZAR
            _CONEXION = new MySqlConnection(CadenaConexion);
            try
            {
                _CONEXION.Open();
                Conectado = true;
            }
            catch
            {
                Conectado = false;
            }
            return Conectado;
        }
        public void Desconectar()
        {
            if (_CONEXION.State == System.Data.ConnectionState.Open)
            {
                _CONEXION.Close();
            }
        }
    }
}
