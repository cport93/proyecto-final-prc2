﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CacheManager
{
    public class Cache
    {
        String _Filtro;

        public String Filtro
        {
            get { return _Filtro; }
            set { _Filtro = value; }
        }

        public enum Consultas
        {
            INICIO_SESION,
            PRIVILEGIOS_ASIGNADOS_SEGUN_IDGRUPO,
            EQUIPOS_INSCRITOS,
            TODOS_LOS_GRUPOS,
            TODOS_LOS_INTEGRANTES,
            TODOS_LOS_USUARIOS,
        }
        public DataTable Obtener(Consultas pConsulta)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Consulta = new StringBuilder();
            switch (pConsulta)
            {
                case Consultas.PRIVILEGIOS_ASIGNADOS_SEGUN_IDGRUPO:
                    Consulta.Append("select p.IDPrivilegio, pr.Privilegio ");
                    Consulta.Append("from permisos p inner join privilegios pr on p.IDPrivilegio = pr.IDPrivilegio ");
                    Consulta.Append("WHERE p.IDGrupo = "+_Filtro+";");
                    break;
                case Consultas.INICIO_SESION:
                    Consulta.Append("SELECT ");
                    Consulta.Append("u.IDUsuario,u.Usuario,u.IDEmpleado,u.IDGrupo, ");
                    Consulta.Append("g.Grupo,e.Nombres,e.Apellidos ");
                    Consulta.Append("FROM sistema.usuarios u,sistema.grupos g,sistema.empleados e ");
                    Consulta.Append("WHERE u.IDGrupo=g.IDGrupos ");
                    Consulta.Append("AND u.IDEmpleado=e.IDEmpleado ");
                    Consulta.Append("AND "+_Filtro+";");
                    break;
                case Consultas.EQUIPOS_INSCRITOS:
                    Consulta.Append("Select * from equipos;");
                    break;
                case Consultas.TODOS_LOS_GRUPOS:
                    Consulta.Append("Select IDGrupos, Grupo from sistema.grupos order by Grupo;");
                    break;
                case Consultas.TODOS_LOS_INTEGRANTES:
                    Consulta.Append("SELECT i.IDIntegrante, i.NombreIntegrante, i.FechaNacimiento, e.NombreEquipo, i.FechaCreacion FROM sistema.integrantes i, sistema.equipos e where i.IDEquipo = e.IDEquipo;");
                        break;
                case Consultas.TODOS_LOS_USUARIOS:
                        Consulta.Append("SELECT * FROM sistema.usuarios;");
                        break;
            }
            try
            {
                DataConexion.DBOperacion Operacion = new DataConexion.DBOperacion();
                Resultado = Operacion.Consultar(Consulta.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.GetBaseException());
            }

            return Resultado;


        }
    }
}
