﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App
{
    class AppManager:ApplicationContext
    {
        //Constructor

        public AppManager()
        {
            //Esta linea se copiara en todos los formularios y clases en los que se utilice variables de sesion
            SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;
            Splash();
            if(Login())
            {
                mdiPrincipal frm = new mdiPrincipal();
                frm.ShowDialog();
            }
        }
        //Splash Screen
        private void Splash()
        {
            frmSplash frm = new frmSplash();
            frm.ShowDialog();
        }
        private Boolean Login()
        {
            Boolean Validado = false;
            frmLogin frm = new frmLogin();
            frm.ShowDialog();
            Validado = frm.Autorizado;
            return Validado;
        }
    }
}
