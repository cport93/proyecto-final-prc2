﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App
{
    public partial class frmLogin : Form
    {
        SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;
        //ATRIBUTO
        Boolean _Autorizado = false;
        //PROPIEDAD DE SOLO LECTURA
        public Boolean Autorizado
        {
            get { return _Autorizado; }
        }

        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void txtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
            {
                txtCredencial.Focus();
            }
        }

        private void txtCredencial_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnEntrar.PerformClick();
            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            DataTable Resultado = new DataTable();
            CacheManager.Cache Consulta = new CacheManager.Cache();
            try
            {
                Consulta.Filtro = "Usuario='" + txtUsuario.Text +"' AND Credencial=md5('"+txtCredencial.Text+"')";
                Resultado = Consulta.Obtener(CacheManager.Cache.Consultas.INICIO_SESION);
                if (Resultado.Rows.Count == 1)
                {
                    _Autorizado = true;
                    _SESION.Usuario = Resultado.Rows[0]["Usuario"].ToString();
                    _SESION.Rol = Resultado.Rows[0]["Grupo"].ToString();
                    _SESION.IDGrupo = Resultado.Rows[0]["IDGrupo"].ToString();
                    _SESION.ObtenerPrivilegios();
                    Close();
                }
                else
                {
                    _Autorizado = false;
                    lblMensaje.Text = "Usuario / Contraseña erroneos";
                    txtCredencial.Focus();
                }
            }
            catch
            {
                _Autorizado = false;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
