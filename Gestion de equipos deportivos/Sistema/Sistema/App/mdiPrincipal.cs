﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataConexion;

namespace App
{
    public partial class mdiPrincipal : Form
    {
        SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;
        private void CargarIndicadores()
        {
            lblUsuario.Text = _SESION.Usuario;
            lblEstacion.Text = Environment.MachineName;
        }
        public mdiPrincipal()
        {
            InitializeComponent();
        }
        private void mdiPrincipal_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            CargarIndicadores();
            if (_SESION.ComprobarPermiso(2))
            {
                usuariosToolStripMenuItem.Enabled = true;
                administrarEquiposToolStripMenuItem.Enabled = true;

            }
            else 
            {
                usuariosToolStripMenuItem.Enabled = true;
                administrarEquiposToolStripMenuItem.Enabled = true;
            }
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
                Mantenimientos.GUI.frmGestionUsuarios frm = new Mantenimientos.GUI.frmGestionUsuarios();
                frm.MdiParent = this;
                frm.Show();
          
        }

        private void administrarEquiposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mantenimientos.GUI.frmGestionEquipos frm = new Mantenimientos.GUI.frmGestionEquipos();
            frm.MdiParent = this;
            frm.Show();

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DBConexion connection = new DBConexion();
            Application.Exit();
        }

        private void agregarIntegrantesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mantenimientos.GUI.fmGestionIntegrantes frm = new Mantenimientos.GUI.fmGestionIntegrantes();
            frm.MdiParent = this;
            frm.Show();
        }

        private void gruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mantenimientos.GUI.frmGestionGrupos frm = new Mantenimientos.GUI.frmGestionGrupos();
            frm.MdiParent = this;
            frm.Show();
        }

        private void clasificatoriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mantenimientos.GUI.frmClasificacion frm = new Mantenimientos.GUI.frmClasificacion();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
